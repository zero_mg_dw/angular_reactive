import {Injectable} from '@angular/core';
// importar los elementos de http y headers para llamada a servicio
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from "rxjs/Observable";
import {timer} from 'rxjs/observable/timer';
import {BehaviorSubject} from "rxjs/BehaviorSubject";
// import de libreria que permite activar el control de eventos del server
import * as EventSource from 'eventsource';

// ----------------------------------------------------------------------------------------------------------------
// el inyectable ayudara a colocar implicitamente una variable con "new" dentro de la clase que usará dicho servicio
// (ver app.component.ts)
// esta anotacion permitira colocar o declarar la variable dentro del constructor
// ----------------------------------------------------------------------------------------------------------------
@Injectable()
export class TweetService {

    // ----------------------------------------------------------------------------------------------------------------
    constructor(private http: HttpClient) {

    }

    // ----------------------------------------------------------------------------------------------------------------
    // servicio que consume directamente del restful reactivo que esta constantñemente verificando cambios en la BD
    // este servicio siempre estará escuchando cualquier cambio de la BD, sea por app o directamente sobre la BD
    // SERVER EVENTS: este servicio corresponde a una de las formas de tener reactividad
    public getTweetFromReactorMongoDriver(): Observable<any> {

        return Observable.create((observer) => {

            let url = 'http://localhost:8080/tweets/reactorDriver'

            const eventSource = new EventSource(url);
            eventSource.onmessage = (event) => {
                //console.log('eventSource.onmessage: ', event);
                const json = JSON.parse(event.data);
                observer.next(json);
            };

            eventSource.onerror = (error) => {
                //console.log("on error ...");
                observer.error('eventSource.onerror: ' + error);
            };

            return () => eventSource.close();
        });
    }

    // ----------------------------------------------------------------------------------------------------------------
    // A diferencia del anterior .... este canal unicamente será canal de suscripción cuando cualquier usuario guarde un registro en la BD
    // SERVER EVENTS: este servicio corresponde a una de las formas de tener reactividad
    public getTweetFromBroadcaster(): Observable<any> {

        return Observable.create((observer) => {

            let url = 'http://localhost:8080/tweets/receiveFromStream'

            const eventSource = new EventSource(url);
            eventSource.onmessage = (event) => {
                //console.log('eventSource.onmessage: ', event);
                const json = JSON.parse(event.data);
                observer.next(json);
            };

            eventSource.onerror = (error) => {
                //console.log("on error ...");
                observer.error('eventSource.onerror: ' + error);
            };

            return () => eventSource.close();
        });
    }


    // ----------------------------------------------------------------------------------------------------------------
    // servicio cuyo proposito es enviar un nuevo tweet a la base de datos
    public sendTweet(tweetData: any): Observable<any> {

        // url donde se encuentra el servicio restful activo (servicio springboot con puerto en application.yml)
        let url: string = "http://localhost:8080/tweets/sendToStream";
        // headers que indican que tipo de contenido se esta enviando, y el tipo de datos que retornará
        let headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        });

        console.log("invocando servicio web ....");
        // observar que se retorna la llamada POST (retorno de observable)
        return this.http.post(url, JSON.stringify(tweetData), {headers: headers});

    }

    // ----------------------------------------------------------------------------------------------------------------


}