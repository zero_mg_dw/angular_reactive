import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
// import de Forms module para uso de "ngModel" dentro des templates html
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

import {AppRoutingModule} from './app-routing.module';

import {AppComponent} from './app.component';
import {PersonService} from "./person.service";
import {TweetService} from "./tweet.service";


@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        // agregado de forms module
        FormsModule,
        // agregado de client HTTP
        HttpClientModule
    ],
    providers: [
        // agregado de "servicio" que nos ayudara en el uso de los POST hacia el servicio RESTful
        PersonService,
        // agregado de "servicio" que nos ayudara en el uso de los POST hacia el servicio RESTful
        TweetService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
