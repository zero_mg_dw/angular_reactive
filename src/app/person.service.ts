import {Injectable} from '@angular/core';
// importar los elementos de http y headers para llamada a servicio
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from "rxjs/Observable";

import {timer} from 'rxjs/observable/timer';
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import * as EventSource from 'eventsource';

// el inyectable ayudara a colocar implicitamente una variable con "new" dentro de la clase que usará dicho servicio
// (ver app.component.ts)
// esta anotacion permitira colocar o declarar la variable dentro del constructor
@Injectable()
export class PersonService {

    public observable$: BehaviorSubject<any> = new BehaviorSubject<any>({});
    public customersList = new Array();

    private sourceTimer = timer(1000,1000);


    getCustomersList(): Observable<any> {
        this.customersList = new Array();

        return Observable.create((observer) => {

            const eventSource = new EventSource('http://localhost:8080/tweets');
            eventSource.onmessage = (event) => {
                console.log('eventSource.onmessage: ', event);
                const json = JSON.parse(event.data);
                this.customersList.push({});
                observer.next(this.customersList);
            };

            eventSource.onerror = (error) => {
                console.log("on error ...");
                observer.error('eventSource.onerror: ' + error);
            }

            return () => eventSource.close();
        });
    }


    constructor(private http: HttpClient) {
        let url: string = "http://localhost:8080/tweets";

       let headers = new HttpHeaders({
            'Accept': 'application/json'
        });

       // console.log("invocando servicio web en constructor ....");


       /* this.sourceTimer.subscribe(time => {
            console.log(time);*/

         /*   this.http.get(url, {headers: headers}).subscribe((result) => {
            //this.http.get(url, {}).subscribe((result) => {
                //console.log("DATA TWEET FROM SERVER:" + JSON.stringify(result));
                this.observable$.next(result);
            });*/
        /*})*/


    }


    // metodo que invocara el servicio restful
    // sera una funcion que retorne el observable de respuesta del servicio restful
    /**
     * FUNCION
     * Nombre: getPersona
     * parametros: personData = objeto json que se enviara a server
     * return: retorna un Observable de la llamada POST
     */
    public getPersona(personData: any): Observable<any> {

        // url donde se encuentra el servicio restful activo (servicio springboot con puerto en application.yml)
        let url: string = "http://localhost:8888/persona";
        // headers que indican que tipo de contenido se esta enviando, y el tipo de datos que retornará
        let headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        });


        console.log("invocando servicio web ....");
        // observar que se retorna la llamada POST (retorno de observable)
        return this.http.post(url,
            JSON.stringify(personData),
            {headers: headers});
    }

    public getEstudiante(personData: any): Observable<any> {

        let url: string = "http://localhost:8585/estudiantes/all";
        let headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        });
        return this.http.get(url,{headers: headers});
    }


    public sendTweet(tweetData: any) {

        // url donde se encuentra el servicio restful activo (servicio springboot con puerto en application.yml)
        let url: string = "http://localhost:8080/tweets";
        // headers que indican que tipo de contenido se esta enviando, y el tipo de datos que retornará
        let headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        });

        console.log("invocando servicio web ....");
        // observar que se retorna la llamada POST (retorno de observable)
        this.http.post(url, JSON.stringify(tweetData), {headers: headers}).subscribe(data => {
                console.log("on subscription post....");
                this.observable$.next(data);
            }
        );
    }


    public getTweets(): Observable<any> {

        // url donde se encuentra el servicio restful activo (servicio springboot con puerto en application.yml)
        let url: string = "http://localhost:8080/tweet";
        // headers que indican que tipo de contenido se esta enviando, y el tipo de datos que retornará
        let headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        });

        console.log("invocando servicio web ....");
        // observar que se retorna la llamada POST (retorno de observable)
        return this.http.get(url, {headers: headers});
    }

}