import {Component, OnInit} from '@angular/core';
// importar el servicio inyectable ..... servicio que hace llamado servicio web
import {PersonService} from "./person.service";

// importar el servicio inyectable ..... servicio que hace llamado servicio web
import {TweetService} from "./tweet.service";
import {HttpResponse} from '@angular/common/http';
import {BehaviorSubject} from "rxjs/BehaviorSubject";

@Component({
    selector: 'p3-root',
    templateUrl: './app.component.html',
    styles: []
})
export class AppComponent implements OnInit {
    title = 'p3';

    public formData: any = {
        firstname: '',
        lastname: ''
    };


    // lista de eventos cambiantes
    public listResult = new Array<any>();


    // Elemento observable que permite el update de la vista al momento de recibir un NEXT
    public _data = new BehaviorSubject<any[]>([]);


    // Person service agregado dentro del constructor de clases (inyectado)
    constructor(private servicioPersona: PersonService,
                private tweetService: TweetService) {
        /*servicioPersona.observable$.subscribe((data) => {
            this.handlerSubscription(data);
        })*/


    }

    public onButtonClick(): void {
        console.log("Elementos de formulario:" + JSON.stringify(this.formData));
        // al haber sido inyectado, no será necesario instanciar el elemento
        // NOTAR EL USO DE SUSCRIPCION PARA OBSERVABLES HTTP RETORNADO POR EL SERVICIO
        // subscriber ..... resulFromService = variable retornada
        this.servicioPersona
            .getPersona(this.formData)
            .subscribe(
                resultFromService => {
                    console.log("Resultado de server:" + JSON.stringify(resultFromService))
                });
    }

    public onButtonClick02(): void {
        console.log("sending new tweet to server...");
        this.tweetService.sendTweet({
            text: "demotweetFromApp",
            createdAt: "2018-01-16T23:28:56.782Z"
        }).subscribe((responseFromServer) => {
            console.log('Result save from server:' + JSON.stringify(responseFromServer));
        });
    }

    public handlerForDirectDriver(data: any): void {
        console.log("on handler direct driver ...:" + JSON.stringify(data));
        this._data.next(data);
    }


    public handlerForBroadcaster(data: any): void {
        console.log("on handler broadcaster...:" + JSON.stringify(data));
        this._data.next(data);
    }

    public handlerHtmlLisData(data: any): void {
        this.listResult.push(data);
    }


    ngOnInit() {
        this.formData = {
            firstname: '',
            lastname: ''
        };

        this.servicioPersona
            .getEstudiante(null)
            .subscribe(
                nextFromServer => {
                    console.log(JSON.stringify(nextFromServer));
                }
            )

        /* this.tweetService.getTweetFromReactorMongoDriver().subscribe((response) => {
             this.handlerForDirectDriver(response);
         });*/


        this.tweetService.getTweetFromBroadcaster().subscribe((response) => {
            this.handlerForBroadcaster(response);
        });

        // subscripcion hacia observable para actualización de lista de eventos html
        this._data.subscribe((data) => {
            this.handlerHtmlLisData(data);
        });
        /*this._data.subscribe((data) => {
            this.miOtroMetodoSuscrito(data);
        });*/


    }
}
